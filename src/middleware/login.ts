import jwt from 'jsonwebtoken';

export default function loginMiddleware (request, response, next) {

    const { authorization } = request.headers;

    if (!authorization){
        return response.sendStatus(401);
    }

    const token = request.headers.authorization.split(" ")[1]

    try{
        const decodificado = jwt.verify(token, 'secreto')
        // console.log(decodificado)
        return next()
    } catch (err) {
        console.log('error.message :>>', err.message)
    }
}