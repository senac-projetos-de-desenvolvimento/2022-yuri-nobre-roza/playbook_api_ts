import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import Autor from './Autor'
import Editora from './Editora'
import Genero from './Genero'
import GeneroTroca from './GeneroTroca'
import Usuario from './Usuario'

@Entity('livro')
export default class Livro {
    
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    isbn: string

    @Column()
    titulo: string
    
    @Column()
    paginas: number

    @Column()
    estado: string

    @Column()
    capa: string

    @ManyToOne(type => Autor, livros => Livro , { eager: true })
    autor: Autor
    
    @ManyToOne(type => Editora, livros => Livro, {eager: true})
    editora: Editora
    
    @ManyToOne(type => Genero, livros => Livro, { eager: true })
    genero: Genero

    @ManyToOne(type => GeneroTroca, livros => Livro, { eager: true})
    generoTroca: GeneroTroca
    
    @ManyToOne(type => Usuario, livro => Livro)
    usuario: Usuario
}