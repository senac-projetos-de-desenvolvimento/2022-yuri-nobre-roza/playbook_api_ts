import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Livro from './Livro';

@Entity('autor')
export default class Autor {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    nome: string

    @Column()
    nacionalidade: string

    @OneToMany(type => Livro, autor => Autor)
    livros: Livro[]
}