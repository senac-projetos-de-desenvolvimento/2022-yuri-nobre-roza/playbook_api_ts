import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Livro from './Livro';

@Entity('genero')
export default class Genero{
    
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    nome: string

    @OneToMany(type => Livro, genero => Genero)
    livros: Livro[]

}