import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Usuario from './Usuario';

@Entity('cidade')
export default class Cidade{

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    nome: string

    @OneToMany(type => Usuario, cidade => Cidade)
    usuarios: Usuario[]
    
}