import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('estado')
export default class Estado {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    nome: string

    @Column()
    sigla: string

}