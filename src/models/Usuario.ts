import { AfterLoad, BeforeInsert, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import bcrypt from 'bcryptjs';
import Cidade from './Cidade';
import Livro from './Livro';
@Entity('usuario')
export default class Usuario {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    nome: string

    @Column()
    sobrenome: string

    @Column()
    email: string

    @Column()
    senha: string;

    @Column()
    endereco: string

    @Column()
    cep: string

    @Column({
      nullable: true
    })
    admin: boolean

    @ManyToOne(type => Cidade, usuarios => Usuario, { eager: true })
    cidade: Cidade

    @OneToMany(type => Livro, usuario => Usuario)
    livro: Livro[]

    verificaSenha(senhaDescripto : string) {
      return bcrypt.compareSync(senhaDescripto, this.senha)
    }
}