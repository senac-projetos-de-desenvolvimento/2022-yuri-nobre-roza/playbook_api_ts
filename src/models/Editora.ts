import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Livro from './Livro';

@Entity('editora')
export default class Editora {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    nome: string

    @OneToMany(type => Livro, editora => Editora)
    livros: Livro[]
    
}