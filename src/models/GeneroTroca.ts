import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import Livro from './Livro'

@Entity('generoTroca')
export default class GeneroTroca{
    
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    nome: string

    @OneToMany(type => Livro, generoTroca => GeneroTroca)
    livros: Livro[]
}