import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateLivro1655850367895 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'livro',
                columns: [
                    {
                        name: 'id',
                        type: 'uuid',
                        isPrimary: true,
                        generationStrategy: 'uuid',
                        default: 'uuid_generate_v4()'
                    },
                    {
                        name: 'isbn',
                        type: 'varchar'
                    },
                    {
                        name: 'paginas',
                        type: 'integer'
                    },
                    {
                        name: 'estado',
                        type: 'varchar'
                    },
                    {
                        name: 'capa',
                        type: 'varchar'
                    },
                ]
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable('livro')
    }

}
