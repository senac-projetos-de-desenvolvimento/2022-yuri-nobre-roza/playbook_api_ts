import {MigrationInterface, QueryRunner} from "typeorm";

export class RelacaoLivroUsuario1657674436407 implements MigrationInterface {
    name = 'RelacaoLivroUsuario1657674436407'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" ADD "usuarioId" uuid`);
        await queryRunner.query(`ALTER TABLE "livro" ADD CONSTRAINT "FK_adf46bda6a9d084ba2ee72dafcf" FOREIGN KEY ("usuarioId") REFERENCES "usuario"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" DROP CONSTRAINT "FK_adf46bda6a9d084ba2ee72dafcf"`);
        await queryRunner.query(`ALTER TABLE "livro" DROP COLUMN "usuarioId"`);
    }

}
