import {MigrationInterface, QueryRunner} from "typeorm";

export class RelacaoLivroGenero1657671251285 implements MigrationInterface {
    name = 'RelacaoLivroGenero1657671251285'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" ADD "generoId" uuid`);
        await queryRunner.query(`ALTER TABLE "livro" ADD CONSTRAINT "FK_5262bb875930c835dde63c2a59d" FOREIGN KEY ("generoId") REFERENCES "genero"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" DROP CONSTRAINT "FK_5262bb875930c835dde63c2a59d"`);
        await queryRunner.query(`ALTER TABLE "livro" DROP COLUMN "generoId"`);
    }

}
