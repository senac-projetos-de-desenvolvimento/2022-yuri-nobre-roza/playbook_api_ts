import {MigrationInterface, QueryRunner} from "typeorm";

export class RelacaoLivroAutor1657669814082 implements MigrationInterface {
    name = 'RelacaoLivroAutor1657669814082'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" ADD "autorId" uuid`);
        await queryRunner.query(`ALTER TABLE "livro" ADD CONSTRAINT "FK_d591802ea54b1c8a09abeb80cec" FOREIGN KEY ("autorId") REFERENCES "autor"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" DROP CONSTRAINT "FK_d591802ea54b1c8a09abeb80cec"`);
        await queryRunner.query(`ALTER TABLE "livro" DROP COLUMN "autorId"`);
    }

}
