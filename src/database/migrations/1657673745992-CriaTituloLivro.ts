import {MigrationInterface, QueryRunner} from "typeorm";

export class CriaTituloLivro1657673745992 implements MigrationInterface {
    name = 'CriaTituloLivro1657673745992'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" ADD "titulo" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" DROP COLUMN "titulo"`);
    }

}
