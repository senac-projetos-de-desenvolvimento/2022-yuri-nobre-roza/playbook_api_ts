import {MigrationInterface, QueryRunner} from "typeorm";

export class RelacaoGenTrocaLivro1665581742534 implements MigrationInterface {
    name = 'RelacaoGenTrocaLivro1665581742534'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" ADD "generoTrocaId" uuid`);
        await queryRunner.query(`ALTER TABLE "livro" ADD CONSTRAINT "FK_17ac86ec242b27fa06bdbcc90e3" FOREIGN KEY ("generoTrocaId") REFERENCES "generoTroca"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" DROP CONSTRAINT "FK_17ac86ec242b27fa06bdbcc90e3"`);
        await queryRunner.query(`ALTER TABLE "livro" DROP COLUMN "generoTrocaId"`);
    }

}
