import {MigrationInterface, QueryRunner} from "typeorm";

export class RelacaoLivroEditora1657670924885 implements MigrationInterface {
    name = 'RelacaoLivroEditora1657670924885'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" ADD "editoraId" uuid`);
        await queryRunner.query(`ALTER TABLE "livro" ADD CONSTRAINT "FK_d6d849ca4a6ab2c41710cf27372" FOREIGN KEY ("editoraId") REFERENCES "editora"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "livro" DROP CONSTRAINT "FK_d6d849ca4a6ab2c41710cf27372"`);
        await queryRunner.query(`ALTER TABLE "livro" DROP COLUMN "editoraId"`);
    }

}
