import {MigrationInterface, QueryRunner} from "typeorm";

export class AttUsuario1655849913482 implements MigrationInterface {
    name = 'AttUsuario1655849913482'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "usuario" ADD "admin" boolean NOT NULL DEFAULT FALSE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "usuario" DROP COLUMN "admin"`);
    }

}
