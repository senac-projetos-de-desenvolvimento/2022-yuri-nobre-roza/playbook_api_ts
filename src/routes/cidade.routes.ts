import { Router } from 'express';
import { getRepository } from 'typeorm';
import Cidade from '../models/Cidade';

const cidadeRouter = Router();

cidadeRouter.post('/', async (request, response) => {
    try{
        const repo = getRepository(Cidade);
        const res = await repo.save(request.body)
        return response.status(201).json(res)
    } catch (err) {
        console.log('error.message :>>', err.message)
    }
})

cidadeRouter.get('/', async (request, response) => {
    response.json(await getRepository(Cidade).find())
})

export default cidadeRouter;