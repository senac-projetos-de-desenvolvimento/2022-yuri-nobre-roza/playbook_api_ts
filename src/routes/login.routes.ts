import { Router } from "express";
import { getRepository } from "typeorm";
import Usuario from "../models/Usuario";
import bcrypt from "bcryptjs";
import jwt  from "jsonwebtoken";

const loginRouter = Router();

loginRouter.post('/', async (request, response) => {
    try {

        const repo = getRepository(Usuario)
        const {email, senha} = request.body;
        
        const validaEmail = await repo.findOneOrFail({ where: {email}})

        if (!validaEmail){
            return response.sendStatus(401)
         }
        
        
        if(!validaEmail.verificaSenha(senha)) {
            return response.sendStatus(401)
        }

        const token = jwt.sign({id: validaEmail.id}, 'secreto', {expiresIn: '3d'})

        delete validaEmail.senha
        
        return response.status(200).json({validaEmail, token})


    } catch(err) {
        console.log('error.message :>>', err.message)
    }
})

export default loginRouter;