import { Router } from 'express';
import { getRepository } from 'typeorm';
import Editora from '../models/Editora';

const editoraRouter = Router();

editoraRouter.post('/', async (request, response) => {
    try{
        const repo = getRepository(Editora);
        const res = await repo.save(request.body)
        return response.status(201).json(res)
    } catch (err) {
        console.log('error.message :>>', err.message)
    }
})

editoraRouter.get('/', async (request, response) => {
    response.json(await getRepository(Editora).find())
})

export default editoraRouter;