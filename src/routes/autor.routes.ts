import { Router } from 'express';
import { getRepository } from 'typeorm';
import Autor from '../models/Autor';

const autorRouter = Router();

autorRouter.post('/', async (request, response) => {
    try{
        const repo = getRepository(Autor);
        const res = await repo.save(request.body);
        return response.status(201).json(res);
    } catch (err) {
        console.log('error.message :>>', err.message)
    }
})

autorRouter.get('/', async (request, response) => {
    response.json(await getRepository(Autor).find())
})

export default autorRouter;