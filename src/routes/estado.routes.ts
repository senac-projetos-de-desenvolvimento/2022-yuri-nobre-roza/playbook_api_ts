import { Router } from 'express';
import { getRepository } from 'typeorm';
import Estado from '../models/Estado';

const estadoRouter = Router();

estadoRouter.post('/', async (request, response) => {
    try{
        const repo = getRepository(Estado)
        const res = await repo.save(request.body)
        return response.status(201).json(res)
    } catch (err) {
        console.log('error.message :>>', err.message)
    }
})

estadoRouter.get('/', async (request, response) => {
    response.json(await getRepository(Estado).find())
})

export default estadoRouter;