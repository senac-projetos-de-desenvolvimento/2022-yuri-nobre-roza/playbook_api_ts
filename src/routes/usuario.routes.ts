import { Router } from 'express';
import { getRepository } from 'typeorm';
import Usuario from '../models/Usuario';

var bcrypt = require('bcryptjs');

const usuarioRouter = Router();

usuarioRouter.post('/', async (request, response) => {
    try {
        const repo = getRepository(Usuario)
        const {nome, sobrenome, email, senha, endereco, cep, admin, cidade} = request.body;

        var hash = await bcrypt.hashSync(senha, 8);

        const User = repo.create({
            nome,
            sobrenome,
            email,
            senha:hash,
            endereco,
            cep,
            admin,
            cidade
        })

        const res = await repo.save(User)
        return response.status(201).json(res)
    } catch (err){
        console.log('error.message :>>', err.message)
    }

})

usuarioRouter.get('/', async (request, response) => {
    response.json(await getRepository(Usuario).find())
})

export default usuarioRouter;