import { Router } from 'express';
import { getRepository } from 'typeorm';
import GeneroTroca from '../models/GeneroTroca';

const generoTrocaRouter = Router ();

generoTrocaRouter.get('/', async (request, response) => {
    response.json(await getRepository(GeneroTroca).find())
})

export default generoTrocaRouter;