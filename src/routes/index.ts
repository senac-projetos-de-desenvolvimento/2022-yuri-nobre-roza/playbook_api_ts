import { Router } from "express";
import cidadeRouter from "./cidade.routes";
import autorRouter from './autor.routes';
import editoraRouter from "./editora.routes";
import generoRouter from "./genero.routes";
import usuarioRouter from "./usuario.routes";
import loginRouter from './login.routes';

import loginMiddleware from "../middleware/login";
import estadoRouter from "./estado.routes";
import livroRouter from "./livro.routes";
import generoTrocaRouter from "./generoTroca.routes";

const routes = Router();

routes.use('/cidade', cidadeRouter);
routes.use('/estado', estadoRouter);
routes.use('/autor', autorRouter);
routes.use('/editora', editoraRouter);
routes.use('/genero', generoRouter);
routes.use('/usuario', usuarioRouter);
routes.use('/login', loginRouter);
routes.use('/livro', livroRouter);
routes.use('/generotroca', generoTrocaRouter);

export default routes;