import { Router } from "express";
import { getRepository } from "typeorm";
import Livro from "../models/Livro";

const livroRouter = Router();

livroRouter.post('/', async (request, response) => {
    try {
        const repo = getRepository(Livro)
        const {titulo, isbn, paginas, estado, capa, autor, editora, genero, generoTroca, usuario} = request.body

        const Lvr = repo.create({
            titulo,
            isbn,
            paginas,
            estado,
            capa,
            autor,
            editora,
            genero,
            generoTroca,
            usuario
        })

        const res = await repo.save(Lvr)
        response.status(201).json(res)
    

    } catch(err) {
        console.log('error.message :>>', err.message)
    }
})


livroRouter.get('/', async (request, response) => {
    try {
        const query = await getRepository(Livro).query(`SELECT * FROM livro`)

        return response.status(200).json(query)
    } catch(err) {
        console.log('error.message :>>', err.message)
    }
})

livroRouter.get('/:id', async (request, response) => {
    try {
        const repo = getRepository(Livro)
        

        const res = await repo.find({
            where: {
                usuario: request.params.id
            }
        })

        return response.status(200).json(res)

    } catch(err) {
        console.log('error.message :>>', err.message)
    }
})

livroRouter.get('/:id/buscatroca', async (request, response) => {
    try {
        const repo = getRepository(Livro)

        const livro = await repo.find({
            where: {
                generoTroca: request.params.id
            }
        })

        return response.json(livro)

    } catch(err) {
        console.log('error.message :>>', err.message)
    }
})
export default livroRouter;