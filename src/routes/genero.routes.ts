import { Router } from 'express';
import { getRepository } from 'typeorm';
import Genero from '../models/Genero';
import GeneroTroca from '../models/GeneroTroca';

const generoRouter = Router();

generoRouter.post('/', async (request, response) => {
    try{
        const repogen = getRepository(Genero);
        const repogentr = getRepository(GeneroTroca);

        const resgen = await repogen.save(request.body)
        const resgentr = await repogentr.save(request.body)

        return response.status(201).json([resgen, resgentr])
    } catch (err) {
        console.log('error.message :>>', err.message)
    }
})

generoRouter.get('/', async (request, response) => {
    response.json(await getRepository(Genero).find())
})

export default generoRouter;