Esse projeto foi desenvolvido usando Typescript, TypeORM e PostgreSQL.
Está sendo utilizada uma database chamada playbook e as conexões com o banco estão rodando na porta 3001 - então libere a porta ou mude a configuração para utilizar outra porta.
Para rodar o projeto faça o download do repositório abra o terminal (via VSCode ou pelo cmd) e siga os passos abaixo:

1) npm i - para adicionar as dependências do projeto
2) criação da database via gerenciador de banco (DBeaver, PGAdmin, etc)
3) é necessário rodar as migration: npm run typeorm migration:run "nome da migration"
4) npm run dev para dar início ao servidor

Importante: É preciso rodar as migrações para criar as tabelas do banco de dados.
Para ter acesso a todas funcionalidades (como o app ainda não está hospedado) é preciso criar um usuário com o parâmetro de "admin" como "true" a força - via Insomnia, Postman ou direto no banco de dados (por padrão o cadastro de usuário passa nulo ao persistir no banco).
Para melhor uso dessa API o front-end projetado para ela está disponível em: https://gitlab.com/senac-projetos-de-desenvolvimento/2022-yuri-nobre-roza/playbook_web
